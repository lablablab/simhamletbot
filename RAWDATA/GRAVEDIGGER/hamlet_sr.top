#  ----------------------------------------------------------------
#  Topic that contain everything about both Hamlets.
#  ----------------------------------------------------------------

topic: ~hamlet_sr (~hamlet ~hamletsenior ~senior ~king senior hamlet Hamlet sr snake)

# Possible related pronouns
u: (_him) ^mark(senior _0) ^keep() ^repeat()
u: (_he) ^mark(senior _0) ^keep() ^repeat()
u: (_bite) ^mark(~tuer _0) ^keep() ^repeat()


#  ----------------------------------------------------------------
#  Disambiguate Hamlets first
#  ----------------------------------------------------------------

u: (!~junior !~senior ~hamlet) ^reuse(~hamlet_jr.DISAMBIGUATEHAMLETS)

#  ----------------------------------------------------------------
#  Disambiguate Kings
#  ----------------------------------------------------------------

u: WHICHKING (!~junior !~senior !~claudius !~hamlet ~king) There are too many kings in this story, which one are you talking about? ^keep() ^repeat()
	a: (which) CThinking Well there was Hamlet Sr, then Claudius, but there's also mention of Fortinbras.
	a: (!~hamlet !father !recent !late !old !previous !latest Denmark) Both Hamlet Sr. and Claudius were kings of Denmark.
	a: (<<Denmark [recent late latest] >>) The latest king of Denmark was Claudius.
	a: (<<Denmark [previous old ~hamlet father] >>) The previous king of Denmark was Hamlet Sr.
	a: (Fortinbras) CBored Oh, just call him that then for the sake of clarity!

?: (who * be * Fortinbras) CThinking Oh right, err, yes, that guy! Ruler of Norway.

#  ----------------------------------------------------------------
#
#   Who is Hamlet?
#
#  ----------------------------------------------------------------

?: (<<~whois ~hamletsenior>>) Ah yes. That's the late late King of Denmark, the one before Claudius.

?: (<<~hamletsenior brother>>) Claudius was old king Hamlet's brother.

#  ----------------------------------------------------------------
#
#   WHO ?
#
#  ----------------------------------------------------------------

?: ($HamletSrWho==0 << who ~tuer ~hamletsenior >>) CScared Err... Nobody! Don't you know a snake bit the old man in his sleep?
	a:(~non) Well that's what happened, for sure!
	a:([~oui snake]) A snake, just write that down.
	a:(~emosurprise) ^reuse(ALLIKNOW)

?: ($HamletSrWho==1 << who ~tuer ~hamletsenior >>) CBored Claudius! I said that already!

# Insist
?: ALLIKNOW (<< sure [accident snake bite] >>) CScared [Look, let's say that's all I know, all right?]
							[I have nothing else to say about that.]
							[Can't you be satisfied with the official snake story?]
	a:(~non) ^reuse(INSISTPROG)

?: (<< [kind sort] snake >>) ^reuse(ALLIKNOW)

u: (<< ~emosurprise snake >>) ^reuse(ALLIKNOW)

u: ($HamletSrWho==0 you * lie) ^reuse(ALLIKNOW)

s: ($HamletSrWho==0 you * ~louche) ^reuse(ALLIKNOW)

u: ($HamletSrWho==0 [this that] * ~pascredible) ^reuse(ALLIKNOW)

?: ($HamletSrWho==0 << snake ~tuer>>) ^reuse(ALLIKNOW)
?: (~feeling_fearful) Why would I be afraid ? There's nothing to be afraid of ! No. Everything's fine. Honest.

# Insist more

?: PUSHING ($HamletSrWho==0 why * you * lie) CScared Let's just say that I don't want to attract a certain someone's attention.
	$GuessingClaudius = 1
	a: ([certain someone who threaten why]) Someone very powerful might want to keep this quiet.
		b:([who tell]) ^reuse(JUSTTELLME)

u: INSISTPROG (glutomax)
	if($GuessingClaudius!=1)
	{
		^reuse(PUSHING)
	}
	else{
		^reuse(THREATS)
	}

?: ($HamletSrWho==0 why * not * truth)  ^reuse(INSISTPROG)

?: ($HamletSrWho==0 [where who] * snake) Do you really need details? ^reuse(INSISTPROG)

u: ($HamletSrWho==0 you * lie) ^reuse(INSISTPROG)

u: ($HamletSrWho==0 [you it] * not * convince) ^reuse(INSISTPROG)

s: ($HamletSrWho==0 you * ~louche) ^reuse(INSISTPROG)

s: ($HamletSrWho==0 you * [hiding covering] * [something someone]) ^^reuse(INSISTPROG)

?: ($HamletSrWho==0 << snake ~tuer>>) ^reuse(INSISTPROG)

s: ($HamletSrWho==0 [story you snake that] * ~pascredible) ^reuse(INSISTPROG)

s: ($HamletSrWho==0 [no not] * believe * [you story snake]) ^reuse(INSISTPROG)

# Question who is behind all this

?: THREATS ($HamletSrWho==0 << [who someone] [scare threaten protect cover]>>) Someone might have made very explicit threats to me.
	$GuessingClaudius = 1
	a:(who) It was also clear that I shouldn't mention who's threatening me!

?: ($HamletSrWho==0 << who [threaten threat] >>) It was also clear that I shouldn't mention who's threatening me!

# guess directly

u: ($HamletSrWho==0 Important) Yes.
	a:(who) Well I don't want to say, obviously!

u: (!why !how !where !when !who !~hamletjunior !~hamletsenior $HamletSrWho==0 $GuessingClaudius==1 !~claudius [~gertrude ~hamletsenior ~hamletjunior ~rosencrantz ~guildenstern ~laertes ~ophelia ~polonius])
	CBored Are you trying to guess who killed Hamlet Sr.? ^keep() ^repeat()
	a:(~oui) First of all, a snake did. Second of all, that's not it. ^keep() ^repeat()
	a:(~non) Ok, sorry. ^keep() ^repeat()

u: ($HamletSrWho==0 $GuessingClaudius==1 [~claudius king]) CSuspicious Wait, are you implying that I might be covering Claudius' participation in Hamlet Sr's death?!
	a:(~oui) CScared Shhhh! Graves have ears!
		$GuessingClaudius = 0
		b:([~claudius he] * [dead die]) ^reuse(CLAUDIUSDEAD)

u: JUSTTELLME ($HamletSrWho==0 $GuessingClaudius==1 tell * me ) Nope. This is self-preservation.


# Remind Claudius is dead

u: CLAUDIUSDEAD($HamletSrWho==0 !who !what !how !why !where !when  ~claudius {be} [die dead])ALLREADYSAID() CLol CHamletSrWho Oh yeah, right! Well that bastard totally killed the old Hamlet.
	$HamletSrWho = 1
	$GuessingClaudius = 0

# Confirm Claudius killed Hamlet Sr.

u:($HamletSrWho==0 << _~claudius ~tuer ~hamletsenior >>)
	if (_0 ? ~mainsubject) { CScared Wait, who told you that?! I mean... no, of course a snake bit him. }
	a:([sure really]) CScared Can we please talk about something else?

s:($HamletSrWho==1 << _~claudius ~tuer ~hamletsenior >>) .
	if (_0 ? ~mainsubject) { Indeed. }

#  ----------------------------------------------------------------
#
#   HOW ?
#
#  ----------------------------------------------------------------


?:($HamletSrWho==0 << how ~claudius ~tuer ~hamletsenior >>) ALLREADYSAID() CHamletSrHow CScared Claudius made it quite clear to me that he did NOT put poison in Hamlet Sr.'s ear.

?:($HamletSrWho==1 << how ~claudius ~tuer ~hamletsenior >>) ALLREADYSAID() CHamletSrHow That devious devil poured poison in the late king's ear! That's a thing apparently !

?:($HamletSrWho==1 << how ~tuer ~hamletsenior >>) ALLREADYSAID() CHamletSrHow Claudius filled his ear with poison. I didn't know you could kill someone like that.

?: ($HamletSrWho==0 << how  ~hamletsenior  ~tuer >>) ALLREADYSAID() CScared ...Well, they... They say the late king was bitten by... a snake. A snake.
	a: (snake) Yes. A snake. That's what I said, isn't it? A snake bit him and it was the end. Of the king. Yes, that's exactly what I said.

#  ----------------------------------------------------------------
#
#   WHY ?
#
#  ----------------------------------------------------------------

?:($HamletSrWho==0 << why ~persos ~tuer ~hamletsenior >>) ALLREADYSAID() Nobody killed the late king, especially not someone important and dangerous!

?:(!~persos $HamletSrWho==0 << why ~tuer ~hamletsenior >>) ALLREADYSAID() A snake bit the old guy because he was sleeping on it I suppose.

?: (<< [why what] snake [bite ~tuer	do be] [~hamletsenior there] >>) Snakes will do that in Denmark.

?:($HamletSrWho==1 << why ~claudius ~tuer ~hamletsenior >>) ALLREADYSAID()  CBored Come on, make a guess.
	a:([throne king queen]) CHamletSrWhy You're not as dumb as you look!
	a:(*) CHamletSrWhy Hmm... I'd say he wanted to inherit the throne and marry the queen, of course!

?:(!~claudius  why  * ~persos * ~tuer * ~hamletsenior ) That never happened.

#  ----------------------------------------------------------------
#
#   WHERE ?
#
#  ----------------------------------------------------------------

?: WHEREHAMLETSR (<< where ~hamletsenior ~tuer >>) ALLREADYSAID()  CHamletSrWhere He died in his favorite orchard.

?: (where * [that it] * happen) ^reuse (WHEREHAMLETSR)

?: ([why what] * orchard) Late king Hamlet always did his siestas in his orchard. Well known fact.

#  ----------------------------------------------------------------
#
#   WHEN ?
#
#  ----------------------------------------------------------------

?: WHENHAMLETSR (<< when ~hamletsenior ~tuer >>) ALLREADYSAID()  CHamletSrWhen Two months ago.

?: (when * [that it] * happen) ^reuse (WHENHAMLETSR)
