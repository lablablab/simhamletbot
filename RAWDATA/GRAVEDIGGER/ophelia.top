
#  ----------------------------------------------------------------
#  Topic that contain everything about Ophelia.
#  ----------------------------------------------------------------

topic: ~Opheliatopic (~ophelia Ophelia ophelia river drown)

# Debug
s: (topic) Ophelia

# Possible related pronouns
u: (_she) ^mark(Ophelia _0) ^keep() ^repeat()
u: (_her) ^mark(Ophelia _0) ^keep() ^repeat()
u: (_herself) ^mark(Ophelia _0)  ^keep() ^repeat()


#  ----------------------------------------------------------------
#  Who was Ophelia ?
#  ----------------------------------------------------------------

?: (!father !dad !parent << ~whois ~ophelia >>) ALLREADYSAID() Ophelia? Fair maiden, in love with Hamlet jr. she was. Also Laertes' sister.

?: (~ophelia *  love * ~hamletjunior) ALLREADYSAID() Completely infatuated, as they say.

?: (love * serious) CLol It was pretty one-sided to tell you the truth. Poor lass.

?: (~hamletjunior * [love like]  * ~ophelia) Prince Hamlet was hot and then cold, said yes and then no, and sometimes he was in and up for it and then he was just down and out. Very confusing for Ophelia.

?: (<<  ~ophelia  ~father>>) CThinking Her da' was old man Polonius. Odd that she was so pretty too, he had the face of a hippo.

?: (<<  ~ophelia  ~brother>>) She had a brother--Laertes.

#  ----------------------------------------------------------------
#  HOW
#  ----------------------------------------------------------------

# Generic HOW
?: (!~hamletjunior !drown <<[how why] ~ophelia ~tuer>>) She stopped breathing. Was a very bad idea.

?: (how * stop * breath)ALLREADYSAID()  CThinking I suppose it was kind of difficult with all that water in her mouth.

# why not breath
?: (<< why  ~ophelia  breathe >>) How am I supposed to know why people do things? I wasn't there when it happened!

# forced not to breath?

?: THEWATERSTUPID (<< what [make stop] [breathing suffocate] ~ophelia >>) The water in her lungs, I imagine.
	a: (water) ^reuse(THERIVERSTUPID)
	a: (drown) ^reuse(DROWNTRIVIAL)

u: (<< [ someone something] ~ophelia [force stop choke suffocate] [water breath] >>) Yes, you could said that something forced her to stop breathing.
	a:([what who]) ^reuse (THEWATERSTUPID)

# drowning

u: DROWNTRIVIAL (!who !when !where !how <<~ophelia drown>>) ALLREADYSAID() COpheliaHow You make it sound so trivial. Yes, she drowned in the river!



#  ----------------------------------------------------------------
#  Who
#  ----------------------------------------------------------------

# Who did kill Ophelia
?: (<< who  [~tuer drown]  ~ophelia >>)ALLREADYSAID()  CThinking Hmm... I can't really settle whether it's the water, her father or Hamlet.
	a:([father ~polonius]) ^reuse(POLONIUSMOTIVE)
	a:([water river]) ^reuse(WATERMOTIVE)
	a:(~hamletjunior) ^reuse(HAMLETMOTIVE)

# Father motive/culpability

?: POLONIUSMOTIVE (<<[why reason how what do]  [~polonius father]  [~tuer drown] ~ophelia >>) He died suddenly and mysteriously... that sort of parenting will drive one nuts.

# water motive/culpability

?: WATERMOTIVE (<<[why reason how what do]  [water river]  [~tuer drown] ~ophelia >>) Well, it filled her lungs and made it hard for her to breathe.

# Hamlet motive/culpability

?: HAMLETMOTIVE (<<[why reason how what do]  [~hamletjunior river]  [~tuer drown] ~ophelia >>)ALLREADYSAID()  Hamlet's sudden fondness for sarcasm and his utter disregard for her wellbeing was definitely not helpful with her grieving and all.
	a:(%length<4 [why how ~emomisunderstand]) It made her sad, crazy sad.

?: (<<how [that disdain death] kill ~ophelia>>) It doesn’t help keep one’s wits to have one’s world fall apart so suddenly: your father disappearing and your lover disdaining you.

# Confirm suicide
?: (<< [suicide "killed herself"] ~ophelia >>) ^reuse(OPHELIASUICIDE)
u: OPHELIASUICIDE (<< ~ophelia ~expres  >>)ALLREADYSAID() COpheliaWho We'll never know but becoming mad probably didn't help her follow proper bathing security regulations.

#  ----------------------------------------------------------------
#  Why
#  ----------------------------------------------------------------

# Why was Ophelia near the water

?: (<< why ~ophelia [drown suicide herself] >>) ^reuse(WHYRIVER)

?: WHYRIVER (<<why ~ophelia [river water]>>) ALLREADYSAID() She was walking by the river too close, too close, they say because she was mad.

?: (what * ~ophelia * do * [river water]) ^reuse(WHYRIVER)

# Madness

u: (!why !what <<~ophelia ~fou>>) COpheliaWhy That's what they say: ^pick(~fou), ^pick(~fou), ^pick(~fou), though no one is sure what, or who, caused the madness.
	a:([what who]) ^reuse(CAUSEMADNESS)

u: CAUSEMADNESS (<< ~ophelia ~fou >>) ALLREADYSAID()  COpheliaWhy Well, it might have been exacerbated by Hamlet's terrible personality, or her father's terrible ambition...er, death.
	a:([terrible personality]) ^reuse(HAMLETMOTIVE)

#  ----------------------------------------------------------------
#  Where
#  ----------------------------------------------------------------

# water?
?: THERIVERSTUPID ([what which where ~tuer] * [drown water river])ALLREADYSAID()  COpheliaWhere Well, sure, she was immersed in the river. Can't be immersed in a cup of water!

?: OPHELIAWHERE (where *  ~ophelia * [~tuer suffocate breath drown]) ALLREADYSAID() CThinking Well I imagine it was somewhere in the cold water she was immersed in.
	a: ([water immerse river]) ^reuse(THERIVERSTUPID)
	a: (drown) ^reuse(DROWNTRIVIAL)

?: (where * [that it] * happen) ^reuse (OPHELIAWHERE)

#  ----------------------------------------------------------------
#  When
#  ----------------------------------------------------------------

?: OPHELIAWHEN (<< when [~tuer drown] ~ophelia >>)ALLREADYSAID()  COpheliaWhen A few days ago. She's still not totally dry.

?: (when * [that it] * happen) ^reuse (OPHELIAWHEN)
