var chokidar = require("chokidar");
var spawn = require('child_process').spawn;

var chatscript = spawn("chatscript",{stdio:['pipe',process.stdout,process.stderr]});

process.stdin.pipe(chatscript.stdin);

chatscript.stdin.write("bob\n");
//chatscript.stdin.write(":build 0\n");
chatscript.stdin.write(":build 1\n");
chatscript.stdin.write(":reset\n");


chokidar.watch('./RAWDATA/**').on("change",function(event,path){
  chatscript.stdin.write(":build 1\n");
  chatscript.stdin.write(":trace varassign\n");
  chatscript.stdin.write(":reset\n");
});
